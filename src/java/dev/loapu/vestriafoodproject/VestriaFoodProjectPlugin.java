/*
 *     Copyright © 2019 Loapu Developments
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.loapu.vestriafoodproject;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class VestriaFoodProjectPlugin extends JavaPlugin implements Listener
{
	// -------------------------------------------- //
	// LOAD
	// -------------------------------------------- //
	@Override
	public void onLoad()
	{
		this.onLoadPre();
		this.onLoadInner();
		this.onLoadPost();
	}
	
	private void onLoadPre()
	{
		this.logPrefixColored = ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + this.getDescription().getPrefix() + ChatColor.DARK_GRAY + "]" + ChatColor.GRAY;
		this.logPrefixPlain = ChatColor.stripColor(this.logPrefixColored);
	}
	
	private void onLoadInner()
	{
	
	}
	
	private void onLoadPost()
	{
	
	}
	
	// -------------------------------------------- //
	// ENABLE
	// -------------------------------------------- //
	
	@Override
	public void onEnable()
	{
		if ( ! this.onEnablePre()) return;
		this.onEnableInner();
		this.onEnablePost();
	}
	
	private long enableTime;
	public long getEnableTime() { return this.enableTime; }
	
	private boolean onEnablePre()
	{
		this.enableTime = System.currentTimeMillis();
		
		log(ChatColor.DARK_GRAY + "\\\\\\\\\\\\\\\\" + ChatColor.GOLD + "\\\\" + ChatColor.YELLOW + "\\\\\\\\ STARTING PLUGIN ////" + ChatColor.GOLD + "//" + ChatColor.DARK_GRAY + "////////");
		
		// Listener
		Bukkit.getPluginManager().registerEvents(this, this);
		
		return true;
	}
	
	public void onEnableInner()
	{
	}
	
	private void onEnablePost()
	{
		long ms = System.currentTimeMillis() - this.enableTime;
		log(ChatColor.DARK_GRAY + "\\\\\\\\\\\\\\\\" + ChatColor.GOLD + "\\\\" + ChatColor.YELLOW + "\\\\\\\\ START FINISHED (Time: " + ms + "ms) ////" + ChatColor.GOLD + "//" + ChatColor.DARK_GRAY + "////////");
	}
	
	// -------------------------------------------- //
	// DISABLE
	// -------------------------------------------- //
	
	@Override
	public void onDisable()
	{
		this.log("Plugin disabled! Goodbye!");
	}
	
	// -------------------------------------------- //
	// CONVENIENCE
	// -------------------------------------------- //
	
	public void suicide()
	{
		this.log(Level.WARNING,"Plugin will be killed!");
		Bukkit.getPluginManager().disablePlugin(this);
	}
	
	// -------------------------------------------- //
	// LOGGING
	// -------------------------------------------- //
	
	private String logPrefixColored = null;
	private String logPrefixPlain = null;
	
	public void debug(String msg)
	{
		if (VestriaFoodProject.get().getSettings().getBoolean("debug"))
		{
			log("DEBUG", msg);
		}
	}
	public void error(Exception e)
	{
		log(Level.SEVERE, "Looks like we have a " + e.toString() + " here.");
		log(Level.SEVERE, "Surely it is Schnitzel's fault but here is a bit more detail:");
		e.printStackTrace();
	}
	public void log(String prefix, String msg)
	{
		log(Level.INFO, "[" + prefix + "] " + msg);
	}
	public void log(String msg)
	{
		log(Level.INFO, " " + msg);
	}
	public void log(Level level, String msg)
	{
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		if (level == Level.INFO && console != null)
		{
			console.sendMessage(this.logPrefixColored + msg);
		}
		else
		{
			Logger.getLogger("Minecraft").log(level, this.logPrefixPlain + " " + msg);
		}
	}
}
