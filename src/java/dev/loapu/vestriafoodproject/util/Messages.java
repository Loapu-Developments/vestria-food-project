/*
 *     Copyright © 2019 Loapu Developments
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.loapu.vestriafoodproject.util;

public enum Messages
{
	// -------------------------------------------- //
	// ENUMS
	// -------------------------------------------- //
	
	NO_PERMISSION,
	INCORRECT_USAGE,
	NOT_ENOUGH_ARGUMENTS,
	SPACER_LONG,
	SPACER_MEDIUM,
	SPACER_SHORT,
	SPACER_UPPER,
	SPACER_LOWER,
	VFP,
	ATTENTION,
	ADVICE,
	INFO1,
	INFO2,
	INFO3,
	INFO4,
	COMMAND_VFP_UPDATER,
	COMMAND_VFP_SETTINGS,
}
