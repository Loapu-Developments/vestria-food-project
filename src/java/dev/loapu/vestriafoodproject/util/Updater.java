/*
 *     Copyright © 2019 Loapu Developments
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dev.loapu.vestriafoodproject.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.loapu.vestriafoodproject.VestriaFoodProject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Updater
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private String version;
	private LinkedHashMap<String, Object> updater = new LinkedHashMap<>();
	
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static Updater i;
	public static Updater get() { return i; }
	public Updater() { i = this; }
	
	// -------------------------------------------- //
	// GETTERS & SETTERS
	// -------------------------------------------- //
	
	public String getVersion() { return version; }
	
	public LinkedHashMap<String, Object> getUpdaterMap() { return updater; }
	
	public String getString(String setting)
	{
		if (!updater.containsKey(setting))
		{
			return null;
		}
		
		return updater.get(setting).toString();
	}
	
	public boolean getBoolean(String setting)
	{
		if (!updater.containsKey(setting))
		{
			return false;
		}
		
		return Boolean.valueOf(updater.get(setting).toString());
	}
	
	public int getInt(String setting)
	{
		if (!updater.containsKey(setting))
		{
			return 0;
		}
		
		return Integer.valueOf(updater.get(setting).toString());
	}
	
	public void setUpdaterMap(LinkedHashMap<String, Object> updater)
	{
		this.updater = updater;
		this.write();
	}
	
	public void set(String setting, Object value)
	{
		updater.put(setting, value);
		this.write();
	}
	
	public void write() { write("updater.json"); }
	
	@SuppressWarnings("Duplicates")
	public void write(String fileName)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		String jsonString = gson.toJson(this);
		try
		{
			FileWriter fileWriter = new FileWriter(VestriaFoodProject.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + fileName);
			fileWriter.write(jsonString);
			fileWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Updater read() {	return read(false);	}
	
	public static Updater read(boolean resource)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		try
		{
			File tempFile = new File(VestriaFoodProject.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + "tmp.json");
			if (!tempFile.exists()) { Files.copy(VestriaFoodProject.get().getResource("Settings" + File.separator + "updater.json"), tempFile.toPath()); }
			if (!resource) { tempFile =  new File(VestriaFoodProject.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + "updater.json"); }
			BufferedReader br = new BufferedReader(new FileReader(tempFile));
			Updater tempSettings = gson.fromJson(br, Updater.class);
			Files.deleteIfExists(Paths.get(VestriaFoodProject.get().getDataFolder().getAbsolutePath() + File.separator + "Settings" + File.separator + "tmp.json"));
			return tempSettings;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static Updater update()
	{
		// Settings from Resources
		Updater tmpUpdaterLocal = read(true);
		
		// Settings from File
		Updater tmpUpdaterExternal = read();
		
		if (tmpUpdaterExternal == null || tmpUpdaterLocal == null) { return null;	}
		
		tmpUpdaterExternal.write("updater.backup");
		
		LinkedHashMap<String, Object> tmpUpdaterMap = tmpUpdaterLocal.getUpdaterMap();
		
		
		for (Entry<String, Object> e : tmpUpdaterExternal.getUpdaterMap().entrySet())
		{
			if (!tmpUpdaterMap.containsKey(e.getKey())) { continue; }
			tmpUpdaterMap.put(e.getKey(), e.getValue());
		}
		
		tmpUpdaterLocal.setUpdaterMap(tmpUpdaterMap);
		tmpUpdaterLocal.write();
		return tmpUpdaterLocal;
	}
}
