This README will be altered after each plugin restart, so do not worry if you delete it by accident.

If you want to create a new language just copy "en.json" rename it to "<locale>.json" and change the parameters inside the new file.
Please be aware that only the pre-delivered language files will be updated automatically.

The following variables are set:

INCORRECT_USAGE:
    {0} - Hint on how to correct the used command.

SPACER_LOWER,
SPACER_UPPER:
    {0} - Text between the two spacer elements. Gets uppercased. Advice: This counts for each spacer so SPACER_UPPER and SPACER_LOWER have two spacer elements each.

INFO1:
    {0} - Plugin name
    {1} - Plugin version

INFO2:
    {0} - Plugin authors

INFO3:
    {0} - Documentation link

INFO4:
    {0} - Command for plugin help